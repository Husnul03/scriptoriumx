# ScriptoriumX

ScriptoriumX is a game engine that makes it easy for anyone to create games using HTML, JavaScript, and CSS.

## Features

- Visual programming language that is similar to Scratch, but with the ability to use actual code
- Built-in physics engine for realistic game physics
- Support for custom code, so you can add your own functionality as needed
- Easy-to-use interface for adding and manipulating game objects, such as sprites and backgrounds
- Support for HTML, JavaScript, and CSS, so you can create games using web technologies you already know

## Getting Started

To get started with ScriptoriumX, follow these steps:

1. Go to [ScriptoriumX.com](https://www.scriptoriumx.com/).
2. Click the create a project button in the top left corner.
3. Start creating your own games using the visual programming language or by writing your own custom code.

## License

ScriptoriumX is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). This means that you are free to use, modify, and distribute ScriptoriumX, as long as you give attribution to the original work and share any modifications under the same license. For more information, please see the [Creative Commons website](https://creativecommons.org/licenses/by-sa/4.0/).

