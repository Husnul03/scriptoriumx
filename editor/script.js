//FUNCTIONS
let saved = false;
document.addEventListener('keydown', function(event) {
  if (event.altKey && event.code === 'KeyR') {
    runTest();
  } else if (event.ctrlKey && event.code === 'KeyS') {
    event.preventDefault();
    saveProject();
  }
});

function runTest() {
  notify('Playtest session started.', 'alert');
}

function saveProject() {
  notify('Project Data Saved');
}

function reloadEditor() {
  alert('Are you sure you want to clear the editor, all progress will be lost?', 'clearEditor()')
}

function clearEditor() {
  notify('Web Editor Cleared', 'warning');
  const codespace = document.getElementById('codespace');
  codespace.textContent = '';}

//RENAME ITEMS
document.addEventListener('DOMContentLoaded', function() {
const items = document.querySelectorAll('.name');
items.forEach(item => {
  item.addEventListener('dblclick', event => {
    const clicked = event.target;
    clicked.setAttribute('contentEditable', true);
    clicked.classList.add('editing');
    clicked.focus();
    clicked.addEventListener('input', () => {
      const maxLength = 20;
      const content = clicked.textContent;
      if (content.length > maxLength) {
        clicked.textContent = content.slice(0, maxLength);
        clicked.focus();
    const range = document.createRange();
    range.selectNodeContents(clicked);
    range.collapse(false);
    const selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
      }
    });
    clicked.addEventListener('keydown', e => {
      if (e.key === 'Enter') {
        clicked.setAttribute('contentEditable', false);
        clicked.classList.remove('editing');
      }
    });
    clicked.addEventListener('blur', e => {
      clicked.setAttribute('contentEditable', false);
      clicked.classList.remove('editing');
    });
  });
});
});

//Page loading
window.addEventListener('load', function() {
    const loadingScreen = document.querySelector('.loading-screen');
    loadingScreen.classList.add('fadeOut');
    setTimeout(function() {
      loadingScreen.style.display = 'none';
    }, 1000);
  });  
//Leave Page Prompt
window.onbeforeunload = function() {
  if (saved == false && document.getElementById('codespace').textContent != '') {
    return 'Are you sure you want to leave? Your progress may be lost.';
  }
  };

//ONLINE-OFFLINE DETECTION
function handleOnlineStatus() {
  if (window.navigator.onLine) {
    notify('Your back online!')
  } else {
    notify('Your offline, changes wont be saved.', 'warning');
  }
}

window.addEventListener("online", handleOnlineStatus);
window.addEventListener("offline", handleOnlineStatus);

  
//NOTIFICATION SYSTEM
function notify(message, type) {
  // Create a notification element
  const notification = document.getElementById('notifications');
  
  let src = '';

  if (type === 'alert') {
    src = '/assets/icons/alert-icon.png';
  } else if (type === 'warning') {
    src = '/assets/icons/warn-icon.png'
  } else  {
    src = '/assets/icons/noti-icon.png'
  }
  
  notification.innerHTML = `
  <div id="notification-container">
  <img id="notification-icon" src="`+src+`"/>
  <div id="notification-content">`+message+`</div>
  <button id="notification-close-btn">&times;</button></div>`;

  // Set the message text
  
  const closeButton = document.getElementById('notification-close-btn');

  closeButton.addEventListener('click', function() {
    notification.innerHTML = '';
  });
  
  // Set a timeout to remove the notification after 5 seconds
  setTimeout(function() {
    notification.innerHTML = '';
  }, 5000);
}

function alert(message, func) {
  const alert = document.getElementById('alerts');

  alert.innerHTML = `<div id="alert-container">
  <div id="alert-content">`+message+`</div>
  <button id="continue" class="alert-button" onclick="`+func+`">Continue</button>
  <button id="nevermind" class="alert-button">Nevermind</button>
</div>`

const closeButton = document.getElementById('nevermind');
const closeButton2 = document.getElementById('continue');
closeButton.addEventListener('click', function() {
  alert.innerHTML = '';
});
closeButton2.addEventListener('click', function() {
  alert.innerHTML = '';
});
}