/*
░█████╗░░█████╗░███╗░░░███╗███╗░░░███╗░█████╗░███╗░░██╗██████╗░░██████╗
██╔══██╗██╔══██╗████╗░████║████╗░████║██╔══██╗████╗░██║██╔══██╗██╔════╝
██║░░╚═╝██║░░██║██╔████╔██║██╔████╔██║███████║██╔██╗██║██║░░██║╚█████╗░
██║░░██╗██║░░██║██║╚██╔╝██║██║╚██╔╝██║██╔══██║██║╚████║██║░░██║░╚═══██╗
╚█████╔╝╚█████╔╝██║░╚═╝░██║██║░╚═╝░██║██║░░██║██║░╚███║██████╔╝██████╔╝
░╚════╝░░╚════╝░╚═╝░░░░░╚═╝╚═╝░░░░░╚═╝╚═╝░░╚═╝╚═╝░░╚══╝╚═════╝░╚═════╝░
*/
//GET ALL THE FILES
const workspace = document.getElementById('workspace');
const workspaceItems = workspace.querySelector('data');
const scriptElements = dataElement.querySelectorAll('.item [data-type="script"]');



/*
███████╗██╗░░░██╗███╗░░██╗░█████╗░████████╗██╗░█████╗░███╗░░██╗░██████╗
██╔════╝██║░░░██║████╗░██║██╔══██╗╚══██╔══╝██║██╔══██╗████╗░██║██╔════╝
█████╗░░██║░░░██║██╔██╗██║██║░░╚═╝░░░██║░░░██║██║░░██║██╔██╗██║╚█████╗░
██╔══╝░░██║░░░██║██║╚████║██║░░██╗░░░██║░░░██║██║░░██║██║╚████║░╚═══██╗
██║░░░░░╚██████╔╝██║░╚███║╚█████╔╝░░░██║░░░██║╚█████╔╝██║░╚███║██████╔╝
╚═╝░░░░░░╚═════╝░╚═╝░░╚══╝░╚════╝░░░░╚═╝░░░╚═╝░╚════╝░╚═╝░░╚══╝╚═════╝░
*/
//Get Canvas
const canvas = document.getElementById('game-canvas');
const context = canvas.getContext('2d');

//DRAW SHAPES
function drawShape(color, width, height, shape, xPos, yPos, zPos) {
    // Get the canvas element
    const canvas = document.getElementById('my-canvas');
    const context = canvas.getContext('2d');
    
    // Set the fill color and draw the shape
    context.fillStyle = color;
    if (shape === 'rectangle') {
      context.fillRect(xPos, yPos, width, height);
    } else if (shape === 'circle') {
      context.beginPath();
      context.arc(xPos, yPos, width/2, 0, 2*Math.PI);
      context.fill();
    } else {
      console.error('Invalid shape!');
      return;
    }
    
    canvas.style.zIndex = zPos;
  }

//CUSTOM CONSOLE LOG
function cConsoleLog(data) {

}